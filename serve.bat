@echo off
TITLE Running "%~1" Server

REM set NODE_PATH=%cd%

IF "%~1"=="node" GOTO NPM
IF "%~1"=="mongo" GOTO MONGO

REM GOTO EXIT

:NPM
  ECHO Starting Node Server
  REM npm run debug
  REM npm install

  SET count=1

  SETLOCAL ENABLEDELAYEDEXPANSION
    FOR /F "tokens=*" %%g IN ('npm run pm2-status') do (
      SET /a count=!count!+1
    )

    echo count %count%
    IF %count% LEQ 10 (GOTO START) ELSE (GOTO RESTART)

    :START
      ECHO Starting Server
      npm run pm2-start
    GOTO EXIT
    :RESTART
      ECHO Restarting Server
      npm run pm2-restart
    GOTO EXIT
  ENDLOCAL

:MONGO
  ECHO Starting MongoDB Server
  mongod --config ./config/mongod.conf
GOTO EXIT

:EXIT
  ECHO Exiting "%~1" Server
  pause > nul
