const mongoose = require("mongoose");
const mediaModel = mongoose.model("media");
const path = require("path");

const helper = require(__basedir + "/api/helper");
const send = require("../libs/response-lib");
const ObjectId = mongoose.Types.ObjectId;

module.exports = {
  getById: function(req) {
    console.log("getMediaById", JSON.stringify(req.params, null, 4));

    return new Promise((resolve, reject) => {
      mediaModel.aggregate(
        [
          {
            $match: {
              _id: ObjectId(req.params.mediaId)
            }
          },
          {
            $lookup: {
              from: "users",
              localField: "userId",
              foreignField: "_id",
              as: "user"
            }
          },
          {
            $unwind: "$user"
          },
          {
            $addFields: {
              "user.profileImage": "$user.profileImage.path"
            }
          },
          {
            $project: {
              user: {
                _id: 0,
                password: 0,
                __v: 0,
                verificationToken: 0,
                verified: 0
              }
            }
          }
        ],
        function(err, mediaInfo) {
          if (err) reject(err);
          else {
            if (mediaInfo !== null && mediaInfo.length > 0) {
              mediaInfo = mediaInfo[0];
              let data = {
                medias: {
                  user: mediaInfo.user,
                  fileName: mediaInfo.fileName,
                  name: mediaInfo.name,
                  path: mediaInfo.path,
                  timestamp: mediaInfo.timestamp,
                  type: mediaInfo.type.split("/")[0],
                  thumbnail: mediaInfo.thumbnail,
                  source: mediaInfo.source
                }
              };
              resolve(
                send.success({
                  message: "Media Found",
                  data: data
                })
              );
            } else {
              resolve(
                send.failure({
                  message: "Media Not Found"
                })
              );
            }
          }
        }
      );
    });
  },
  getByTimeStamp: function(req) {
    console.log("getMediaByTimeStamp", JSON.stringify(req.query, null, 4));

    return new Promise((resolve, reject) => {
      let mediasList = [];
      mediaModel.aggregate(
        [
          {
            $match: {
              timestamp: {
                $gte: Number(req.query.timestamp)
              }
            }
          },
          {
            $lookup: {
              from: "users",
              localField: "userId",
              foreignField: "_id",
              as: "user"
            }
          },
          {
            $unwind: "$user"
          },
          {
            $addFields: {
              "user.profileImage": "$user.profileImage.path"
            }
          },
          {
            $project: {
              user: {
                _id: 0,
                password: 0,
                __v: 0,
                verificationToken: 0,
                verified: 0
              }
            }
          }
        ],
        function(err, medias) {
          if (err) next(err);
          else {
            if (medias.length > 0) {
              for (let media of medias) {
                mediasList.push({
                  user: media.user,
                  mediaId: media._id,
                  fileName: media.fileName,
                  name: media.name,
                  path: media.path,
                  timestamp: media.timestamp,
                  type: media.type.split("/")[0],
                  thumbnail: media.thumbnail,
                  source: media.source
                });
              }
              resolve(
                send.success({
                  message: "Media List Found",
                  data: {
                    mediaList: mediasList
                  }
                })
              );
            } else {
              resolve(
                send.failure({
                  message: "No Media Found"
                })
              );
            }
            console.log("mediaList", JSON.stringify(mediasList, null, 4));
          }
        }
      );
    });
  },
  paginate: function(req) {
    console.log("paginate", JSON.stringify(req.query, null, 4));

    return new Promise((resolve, reject) => {
      let mediasList = [],
        limit = Number(req.query.limit),
        page = Number(req.query.page);

      page = limit * (page - 1);

      mediaModel.aggregate(
        [
          {
            $lookup: {
              from: "users",
              localField: "userId",
              foreignField: "_id",
              as: "user"
            }
          },
          {
            $unwind: "$user"
          },
          {
            $addFields: {
              "user.profileImage": "$user.profileImage.path"
            }
          },
          {
            $project: {
              user: {
                _id: 0,
                password: 0,
                __v: 0,
                verificationToken: 0,
                verified: 0
              }
            }
          },
          {
            $skip: page
          },
          {
            $limit: limit
          }
        ],
        function(err, medias) {
          if (err) reject(err);
          else {
            if (medias.length > 0) {
              for (let media of medias) {
                mediasList.push({
                  user: media.user,
                  mediaId: media._id,
                  fileName: media.fileName,
                  name: media.name,
                  path: media.path,
                  timestamp: media.timestamp,
                  type: media.type.split("/")[0],
                  thumbnail: media.thumbnail,
                  source: media.source
                });
              }
              console.log("mediaList", JSON.stringify(mediasList, null, 4));
              resolve(
                send.success({
                  message: "Media List Found",
                  data: {
                    mediaList: mediasList
                  }
                })
              );
            } else {
              resolve(
                send.failure({
                  message: "Media Not Found",
                  data: {
                    mediaList: mediasList
                  }
                })
              );
            }
          }
        }
      );
    });
  },
  lazyLoad: function(req) {
    console.log("lazyLoad", JSON.stringify(req.query, null, 4));

    return new Promise((resolve, reject) => {
      let mediasList = [],
        limit = Number(req.query.limit),
        mediaId = req.query.mediaId ? ObjectId(req.query.mediaId) : 0,
        userId = ObjectId(req.auth.credentials.id);

      mediaModel.aggregate(
        [
          {
            $lookup: {
              from: "users",
              localField: "userId",
              foreignField: "_id",
              as: "user"
            }
          },
          {
            $unwind: {
              path: "$user",
              preserveNullAndEmptyArrays: true
            }
          },
          {
            $lookup: {
              from: "follows",
              localField: "userId",
              foreignField: "userId",
              as: "follows"
            }
          },
          {
            $unwind: {
              path: "$follows",
              preserveNullAndEmptyArrays: true
            }
          },
          {
            $addFields: {
              followersId: "$follows.followersId",
              "user.userId": "$user._id",
              "user.profileImage": "$user.profileImage.path"
            }
          },
          {
            $project: {
              user: {
                _id: 0,
                password: 0,
                __v: 0,
                verificationToken: 0,
                verified: 0
              },
              follows: 0
            }
          },
          {
            $match: {
              $or: [
                {
                  $and: [
                    {
                      followersId: {
                        $elemMatch: { $eq: userId }
                      }
                    },
                    {
                      _id: {
                        $gt: mediaId
                      }
                    }
                  ]
                },
                {
                  userId: userId
                }
              ]
            }
          },
          {
            $limit: limit
          }
        ],
        function(err, medias) {
          if (err) reject(err);
          else {
            if (medias.length > 0) {
              for (let media of medias) {
                mediasList.push({
                  user: media.user,
                  mediaId: media._id,
                  fileName: media.fileName,
                  name: media.name,
                  path: media.path,
                  timestamp: media.timestamp,
                  type: media.type.split("/")[0],
                  source: media.source
                });
              }
              resolve(
                send.success({
                  message: "Media List Found",
                  data: {
                    mediaList: mediasList
                  }
                })
              );
            } else {
              resolve(
                send.failure({
                  message: "Medias Not Found",
                  data: {
                    mediaList: mediasList
                  }
                })
              );
            }
          }
        }
      );
    });
  },
  getAll: function(req) {
    console.log("getAllMedia", JSON.stringify(req.auth, null, 4));

    return new Promise((resolve, reject) => {
      let mediasList = [],
        userId = ObjectId(req.auth.credentials.id);
      mediaModel.aggregate(
        [
          {
            $lookup: {
              from: "users",
              localField: "userId",
              foreignField: "_id",
              as: "user"
            }
          },
          {
            $unwind: {
              path: "$user",
              preserveNullAndEmptyArrays: true
            }
          },
          {
            $lookup: {
              from: "follows",
              localField: "userId",
              foreignField: "userId",
              as: "follows"
            }
          },
          {
            $unwind: {
              path: "$follows",
              preserveNullAndEmptyArrays: true
            }
          },
          {
            $addFields: {
              followersId: "$follows.followersId",
              "user.userId": "$user._id",
              "user.profileImage": "$user.profileImage.path"
            }
          },
          {
            $match: {
              $or: [
                {
                  followersId: {
                    $elemMatch: { $eq: userId }
                  }
                },
                {
                  userId: userId
                }
              ]
            }
          },
          {
            $project: {
              user: {
                _id: 0,
                password: 0,
                __v: 0,
                verificationToken: 0,
                verified: 0
              },
              follows: 0
            }
          }
        ],
        function(err, medias) {
          if (err) reject(err);
          else {
            if (medias.length > 0) {
              for (let media of medias) {
                mediasList.push({
                  user: media.user,
                  mediaId: media._id,
                  fileName: media.fileName,
                  name: media.name,
                  path: media.path,
                  timestamp: media.timestamp,
                  type: media.type.split("/")[0],
                  source: media.source
                });
              }
              resolve(
                send.success({
                  message: "Media List Found",
                  data: {
                    mediaList: mediasList
                  }
                })
              );
            } else {
              resolve(
                send.failure({
                  message: "No Media Found"
                })
              );
            }
            // console.log("mediaList", JSON.stringify(mediasList, null, 4));
          }
        }
      );
    });
  },
  updateById: function(req) {
    console.log("updateMediaById", JSON.stringify(req.payload, null, 4));

    return new Promise((resolve, reject) => {
      mediaModel.findByIdAndUpdate(
        req.payload.mediaId,
        {
          name: req.payload.name
        },
        function(err, mediaInfo) {
          if (err) reject(err);
          else {
            resolve(
              send.success({
                message: "Media Updated Successfully"
              })
            );
          }
        }
      );
    });
  },
  deleteById: function(req) {
    console.log("deleteMediaById", JSON.stringify(req.params, null, 4));
    let fileName = req.params.fileName,
      userId = ObjectId(req.auth.credentials.id);

    return new Promise((resolve, reject) => {
      mediaModel.findOneAndRemove(
        {
          $and: [
            {
              fileName: fileName
            },
            {
              userId: userId
            }
          ]
        },
        function(err, mediaInfo) {
          if (err) reject(err);
          else {
            console.log("media", mediaInfo);
            if (mediaInfo !== null) {
              helper.media.removeMedia(fileName, null, err => {
                resolve(
                  send.success({
                    message: "Media Deleted Successfully"
                  })
                );
              });
            } else {
              resolve(
                send.failure({
                  message:
                    "Unable to Delete Media, Media Not Found Or Do Not Have Priviledge"
                })
              );
            }
          }
        }
      );
    });
  },
  create: function(req) {
    // console.log("uploadMedia", req.payload.file);
    return new Promise((resolve, reject) => {
      let fileArray = [],
        userId = ObjectId(req.auth.credentials.id),
        path = "uploads/",
        uploadFlag = true,
        errMsg =
          "Some Media Rejected: Uploaded Media May Contains Explicit Content";

      let uploadMedia = len => {
        if (len === req.payload.file.length) {
          console.log("**Media Uploaded by**", userId);
          if (uploadFlag) {
            console.log(
              "uploadedMediaDetails",
              JSON.stringify(fileArray, null, 4)
            );
            if (fileArray.length > 0) {
              mediaModel.create(fileArray, function(err, result) {
                if (err) reject(err);
                else {
                  resolve(
                    send.success({
                      message: "Media Uploaded Successfully"
                    })
                  );
                }
              });
            } else {
              resolve(
                send.failure({
                  message: "Please Select Files"
                })
              );
            }
          } else {
            resolve(
              send.failure({
                message: errMsg
              })
            );
          }
        }
      };
      let validateMedia = async (file, index) => {
        let path = "/uploads/temp/",
          fileInfo = helper.file.generateMetadata(file._data, req),
          fileName = fileInfo.fileName,
          fileMeta = fileInfo.fileMeta;

        helper.file.write(path + fileName, file._data, async err => {
          path = "/uploads/";
          if (typeof err === "object") {
            console.log("file.write", err);
          } else {
            let mediaType =
              fileMeta.mime.split("/")[0] === "image" ? true : false;
            console.log("file.fileName", fileName);
            let params = {
              thumbnail: req.payload.thumbnail,
              mediaType: mediaType
            };
            console.log("media", JSON.stringify(params, null, 4));
            await helper.media.verifyContent(params, fileName, null, err => {
              if (err) {
                errMsg = err;
                uploadFlag = false;
              } else {
                let data = {
                  userId: userId,
                  name: file.hapi.filename,
                  fileName: fileName,
                  type: fileMeta.mime,
                  path: path + fileName,
                  timestamp: req.payload.timestamp
                };

                // if (!mediaType) {
                //   if (req.payload.thumbnail) {
                //     data["thumbnail"] = req.payload.thumbnail;
                //   } else {
                //     uploadFlag = false;
                //     errMsg = "Thumbnil Not Provided";
                //   }
                // }

                fileArray.push(data);
              }
              // if (fileArray.length === req.payload.file.length) {
              uploadMedia(fileArray.length);
              // }
            });
          }
        });
      };
      req.payload.timestamp = new Date().getTime();
      req.payload.file.forEach(async (file, index) => {
        await validateMedia(file, index);
      });
    });
  },
  source: function(req) {
    console.log("uploadMediaFromSource", JSON.stringify(req.payload, null, 4));

    return new Promise((resolve, reject) => {
      if (req.payload.url && req.payload.source) {
        let fileUrl = {
          userId: req.payload.userId,
          name: req.payload.name,
          fileName: req.payload.name,
          type: req.payload.mimetype,
          path: req.payload.url,
          timestamp: new Date().getTime(),
          source: req.payload.source
        };
        mediaModel.create(fileUrl, function(err, result) {
          if (err) next(err);
          else {
            console.log("sourceResult", result);
            resolve(
              send.success({
                message: "Media Shared Successfully"
              })
            );
          }
        });
      } else {
        resolve(
          send.failure({
            message: "Missing Required Keys"
          })
        );
      }
    });
  }
};
