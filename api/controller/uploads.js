const mongoose = require("mongoose");
const userModel = mongoose.model("users");
const mediaModel = mongoose.model("media");
// const bcrypt = require("bcrypt");
// const handlebars = require("handlebars");

const helper = require(__basedir + "/api/helper");
const send = require("../libs/response-lib");

const ObjectId = mongoose.Types.ObjectId;

module.exports = {
  fetchMediaUploads: function(req) {
    let userId = ObjectId(req.auth.credentials.id);
    let fileName = req.params.fileName;

    return new Promise((resolve, reject) => {
      mediaModel.aggregate(
        [
          {
            $lookup: {
              from: "users",
              localField: "userId",
              foreignField: "_id",
              as: "user"
            }
          },
          {
            $unwind: {
              path: "$user",
              preserveNullAndEmptyArrays: true
            }
          },
          {
            $lookup: {
              from: "follows",
              localField: "userId",
              foreignField: "userId",
              as: "follows"
            }
          },
          {
            $unwind: {
              path: "$follows",
              preserveNullAndEmptyArrays: true
            }
          },
          {
            $addFields: {
              followersId: "$follows.followersId",
              "user.userId": "$user._id",
              "user.profileImage": "$user.profileImage.path"
            }
          },
          {
            $match: {
              $and: [
                {
                  $or: [
                    {
                      followersId: {
                        $elemMatch: { $eq: userId }
                      }
                    },
                    {
                      userId: userId
                    }
                  ]
                },
                {
                  fileName: fileName
                }
              ]
            }
          },
          {
            $project: {
              user: {
                _id: 0,
                password: 0,
                __v: 0,
                verificationToken: 0,
                verified: 0
              },
              follows: 0
            }
          }
        ],
        function(err, result) {
          if (err) reject(err);
          else {
            result ? resolve(result[0].path) : resolve(new Error("Not Found"));
          }
        }
      );
    });
  },
  fetchProfileImageUploads: function(req) {
    if (req.params.fileName) {
      let fileName = req.params.fileName;
      return new Promise((resolve, reject) => {
        userModel.findOne({ "profileImage.fileName": fileName }, function(
          err,
          result
        ) {
          if (err) reject(err);
          else {
            result
              ? resolve(result.profileImage.path)
              : resolve(new Error("Not Found"));
          }
        });
      });
    } else {
      let userId = req.query.user
        ? ObjectId(req.query.user)
        : ObjectId(req.auth.credentials.id);

      return new Promise((resolve, reject) => {
        userModel.findById(userId, function(err, result) {
          if (err) reject(err);
          else {
            result
              ? resolve(result.profileImage.path)
              : resolve(new Error("Not Found"));
          }
        });
      });
    }
  }
};
