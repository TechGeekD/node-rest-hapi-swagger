"user strict";
const mongoose = require("mongoose");
const followModel = mongoose.model("follows");
const userModel = mongoose.model("users");
const helper = require(__basedir + "/api/helper");
const send = require("../libs/response-lib");

const ObjectId = mongoose.Types.ObjectId;

module.exports = {
  followUser: function(req) {
    console.log("followUser", JSON.stringify(req.payload, null, 4));

    return new Promise((resolve, reject) => {
      followModel.findOne(
        {
          $and: [
            {
              followingId: {
                $elemMatch: { $eq: ObjectId(req.payload.followId) }
              }
            },
            { userId: ObjectId(req.auth.credentials.id) }
          ]
        },
        function(err, result) {
          if (err) reject(err);
          else {
            if (result !== null) {
              console.log("resultFollowModel", JSON.stringify(result, null, 4));

              resolve(
                send.failure({
                  message: "Already Following User"
                })
              );
            } else {
              let bulk = followModel.collection.initializeOrderedBulkOp();
              bulk
                .find({
                  userId: ObjectId(req.auth.credentials.id)
                })
                .upsert()
                .updateOne({
                  $push: {
                    followingId: ObjectId(req.payload.followId)
                  }
                });
              bulk
                .find({
                  userId: ObjectId(req.payload.followId)
                })
                .upsert()
                .updateOne({
                  $push: {
                    followersId: ObjectId(req.auth.credentials.id)
                  }
                });
              bulk.execute(function(err, result) {
                if (err) reject(err);
                else {
                  if (result !== null) {
                    console.log("result", JSON.stringify(result, null, 4));

                    resolve(
                      send.success({
                        message: "User Followed Successfully"
                      })
                    );
                  } else {
                    resolve(
                      send.failure({
                        message: "Error Following User"
                      })
                    );
                  }
                }
              });
            }
          }
        }
      );
    });
  },
  unFollowUser: function(req) {
    console.log("followUser", JSON.stringify(req.payload, null, 4));

    return new Promise((resolve, reject) => {
      followModel.findOne(
        {
          followingId: { $elemMatch: { $eq: ObjectId(req.payload.followId) } }
        },
        function(err, result) {
          if (err) reject(err);
          else {
            if (result !== null) {
              console.log("resultFollowModel", JSON.stringify(result, null, 4));

              let bulk = followModel.collection.initializeOrderedBulkOp();
              bulk
                .find({
                  userId: ObjectId(req.auth.credentials.id)
                })
                .upsert()
                .updateOne({
                  $pull: {
                    followingId: ObjectId(req.payload.followId)
                  }
                });

              bulk
                .find({
                  userId: ObjectId(req.payload.followId)
                })
                .upsert()
                .updateOne({
                  $pull: {
                    followersId: ObjectId(req.auth.credentials.id)
                  }
                });

              bulk.execute(function(err, result) {
                if (err) reject(err);
                else {
                  if (result !== null) {
                    console.log("result", JSON.stringify(result, null, 4));

                    resolve(
                      send.success({
                        message: "User unFollowed Successfully"
                      })
                    );
                  } else {
                    resolve(
                      send.failure({
                        message: "Error unFollowing User"
                      })
                    );
                  }
                }
              });
            } else {
              resolve(
                send.failure({
                  message: "User Not Found In Follow List"
                })
              );
            }
          }
        }
      );
    });
  },
  getFollowList: function(req) {
    console.log("following", JSON.stringify(req.payload, null, 4));

    return new Promise((resolve, reject) => {
      let userId = req.query.user
        ? ObjectId(req.query.user)
        : ObjectId(req.auth.credentials.id);
      followModel.aggregate(
        [
          {
            $lookup: {
              from: "users",
              localField: "followingId",
              foreignField: "_id",
              as: "following"
            }
          },
          {
            $lookup: {
              from: "users",
              localField: "followersId",
              foreignField: "_id",
              as: "followers"
            }
          },
          {
            $match: {
              userId: userId
            }
          },
          {
            $project: {
              following: {
                password: 0,
                __v: 0,
                verificationToken: 0,
                verified: 0
              },
              followers: {
                password: 0,
                __v: 0,
                verificationToken: 0,
                verified: 0
              }
            }
          }
        ],
        function(err, userInfo) {
          if (err) reject(err);
          else {
            if (userInfo !== null) {
              console.log("userInfo", JSON.stringify(userInfo, null, 4));

              let data = {
                following: userInfo[0]
                  ? userInfo[0].following.map(e => {
                      e["userId"] = e._id;
                      e.profileImage = e.profileImage
                        ? e.profileImage.path
                        : null;
                      delete e._id;
                      return e;
                    })
                  : null,
                followers: userInfo[0]
                  ? userInfo[0].followers.map(e => {
                      e["userId"] = e._id;
                      e.profileImage = e.profileImage
                        ? e.profileImage.path
                        : null;
                      delete e._id;
                      return e;
                    })
                  : null
              };

              resolve(
                send.success({
                  message: "List of Follows",
                  data: data
                })
              );
            } else {
              resolve(
                send.failure({
                  message: "Can't Fetch List of Follows"
                })
              );
            }
          }
        }
      );
    });
  },
  getProfileInfo: function(req) {
    return new Promise((resolve, reject) => {
      let userId = req.query.user
        ? ObjectId(req.query.user)
        : ObjectId(req.auth.credentials.id);

      userModel.aggregate(
        [
          {
            $match: {
              _id: userId
            }
          },
          {
            $lookup: {
              from: "media",
              localField: "_id",
              foreignField: "userId",
              as: "medias"
            }
          },
          {
            $lookup: {
              from: "follows",
              localField: "_id",
              foreignField: "userId",
              as: "follows"
            }
          },
          {
            $unwind: {
              path: "$follows",
              preserveNullAndEmptyArrays: true
            }
          },
          {
            $lookup: {
              from: "users",
              localField: "follows.followingId",
              foreignField: "_id",
              as: "following"
            }
          },
          {
            $lookup: {
              from: "users",
              localField: "follows.followersId",
              foreignField: "_id",
              as: "followers"
            }
          },
          {
            $addFields: {
              "following.userId": "$following._id",
              "followers.userId": "$followers._id",
              "medias.mediaId": "$medias._id"
            }
          },
          {
            $project: {
              follows: 0,
              following: {
                _id: 0,
                password: 0,
                __v: 0,
                verificationToken: 0,
                verified: 0
              },
              followers: {
                _id: 0,
                password: 0,
                __v: 0,
                verificationToken: 0,
                verified: 0
              },
              medias: {
                _id: 0,
                thumbnail: 0,
                __v: 0,
                userId: 0
              }
            }
          }
        ],
        function(err, userInfo) {
          console.log("userProfileInfo", JSON.stringify(userInfo, null, 4));

          if (err) reject(err);
          else {
            if (userInfo !== null) {
              let data = {
                userProfile: userInfo.map(e => {
                  return {
                    userId: e._id,
                    fullname: e.fullname,
                    username: e.username,
                    email: e.email,
                    profileImage: e.profileImage ? e.profileImage.path : null,
                    follows: {
                      following: e.following.map(e => {
                        e.userId = e.userId ? e.userId[0] : e.userId;
                        return e;
                      }),
                      followers: e.followers.map(e => {
                        e.userId = e.userId ? e.userId[0] : e.userId;
                        return e;
                      })
                    },
                    medias: e.medias.map(e => {
                      e.mediaId = e.mediaId ? e.mediaId[0] : e.mediaId;
                      return e;
                    })
                  };
                })[0]
              };

              resolve(
                send.success({
                  message: "User Profile Fetched",
                  data: data
                })
              );
            } else {
              resolve(
                send.failure({
                  message: "Error Fetching User Info"
                })
              );
            }
          }
        }
      );
    });
  },
  setProfileImage: function(req) {
    // console.log("uploadProfileImage", req.payload.profileImage);

    return new Promise(async (resolve, reject) => {
      let profileImage = {},
        userId = req.auth.credentials.id,
        path = "/uploads/profileImage/",
        uploadFlag = true,
        errMsg =
          "Some Media Rejected: Uploaded Media Contains Explicit Content",
        deleteFlag = req.query.remove ? true : false;
      req.payload.timestamp = new Date().getTime();
      let uploadMedia = () => {
        console.log("**Media Uploaded by**", userId);

        if (uploadFlag) {
          console.log(
            "uploadedMediaDetails",
            JSON.stringify(profileImage, null, 4)
          );

          if (profileImage) {
            userModel.findByIdAndUpdate(
              userId,
              { profileImage: profileImage },
              function(err, result) {
                if (err) reject(err);
                else {
                  console.log("profileImage", JSON.stringify(result, null, 4));

                  resolve(
                    send.success({
                      message: "Profile Image Updated Successfully"
                    })
                  );
                }
              }
            );
          } else {
            resolve(
              send.failure({
                message: "Please Select Files"
              })
            );
          }
        } else {
          resolve(
            send.failure({
              message: errMsg
            })
          );
        }
      };

      // req.files.userFile.forEach((file, index) => {
      userModel.findByIdAndUpdate(userId, { profileImage: null }, function(
        err,
        result
      ) {
        if (err && deleteFlag) reject(err);
        else {
          helper.media.removeMedia(userId, path, err => {
            if (err) {
              console.log("removeMedia:profileImage", err);
            }
          });
        }
      });
      if (!deleteFlag) {
        if (!req.payload.profileImage) {
          resolve(
            send.failure({ message: "Please Provide File For ProfileImage" })
          );
        }
        let file = req.payload.profileImage,
          fileInfo = await helper.file.generateMetadata(file._data, req),
          fileMeta = fileInfo.fileMeta,
          fileName = fileInfo.fileName,
          mediaType = fileMeta.mime.split("/")[0] === "image" ? true : false;
        console.log("file.fileName", file.hapi.filename);

        helper.file.write("/uploads/temp/" + fileName, file._data, err => {
          if (typeof err === "object") reject(err);
          else {
            let params = {
              thumbnail: req.payload.thumbnail,
              mediaType: mediaType
            };
            helper.media.verifyContent(params, fileName, path, err => {
              if (err) {
                errMsg = err;
                uploadFlag = false;
              } else {
                let data = {
                  name: file.hapi.filename,
                  fileName: fileName,
                  type: fileMeta.mime,
                  path: path + fileName,
                  timestamp: req.payload.timestamp
                };

                if (!mediaType) {
                  if (req.payload.thumbnail) {
                    data["thumbnail"] = req.payload.thumbnail;
                  } else {
                    uploadFlag = false;
                    errMsg = "Thumbnil Not Provided";
                  }
                }

                profileImage = data;
              }
              // if (index === req.files.userFile.length - 1) {
              uploadMedia();
              // }
            });
          }
        });
      } else {
        resolve(
          send.success({
            message: "Profile Removed Successfully"
          })
        );
      }
    });

    // });
  },
  getUsersList: function(req) {
    return new Promise((resolve, reject) => {
      let userListCheck = () => {
        if (req.query.allUser) {
          return {
            $ne: ["$_id", ObjectId(req.auth.credentials.id)]
          };
        } else {
          return {
            $and: [
              { $ne: ["$followersId", ObjectId(req.auth.credentials.id)] },
              {
                $ne: ["$_id", ObjectId(req.auth.credentials.id)]
              }
            ]
          };
        }
      };
      userModel.aggregate(
        [
          {
            $lookup: {
              from: "follows",
              localField: "_id",
              foreignField: "userId",
              as: "follows"
            }
          },
          {
            $unwind: {
              path: "$follows",
              preserveNullAndEmptyArrays: true
            }
          },
          {
            $addFields: {
              followersId: "$follows.followersId",
              followingId: "$follows.followingId"
            }
          },
          {
            $project: {
              password: 0,
              __v: 0,
              verificationToken: 0,
              verified: 0
            }
          },
          // {
          //   $unwind: {
          //     path: "$followersId",
          //     preserveNullAndEmptyArrays: true
          //   }
          // },
          // {
          //   $unwind: {
          //     path: "$followingId",
          //     preserveNullAndEmptyArrays: true
          //   }
          // },
          {
            $redact: {
              $cond: {
                if: {
                  $ne: ["$_id", ObjectId(req.auth.credentials.id)]
                },
                then: "$$KEEP",
                else: "$$PRUNE"
              }
            }
          },
          {
            $addFields: {
              isFollowing: {
                $cond: {
                  if: {
                    $ne: [{ $type: "$followersId" }, "array"]
                  },
                  then: false,
                  else: {
                    $cond: {
                      if: {
                        $in: [ObjectId(req.auth.credentials.id), "$followersId"]
                      },
                      then: true,
                      else: false
                    }
                  }
                }
              },
              isFollowingBack: {
                $cond: {
                  if: {
                    $ne: [{ $type: "$followingId" }, "array"]
                  },
                  then: false,
                  else: {
                    $cond: {
                      if: {
                        $in: [ObjectId(req.auth.credentials.id), "$followingId"]
                      },
                      then: true,
                      else: false
                    }
                  }
                }
              }
            }
          }
        ],
        function(err, userInfo) {
          console.log("userInfo", JSON.stringify(userInfo, null, 4));

          if (err) reject(err);
          else {
            if (userInfo !== null) {
              let data = {
                users: userInfo.map(e => {
                  return {
                    userId: e._id,
                    fullname: `${e.firstName || e.fullname} ${e.lastName ||
                      ""}`,
                    firstName: e.firstName,
                    lastName: e.lastName,
                    username: e.username,
                    email: e.email,
                    profileImage: e.profileImage ? e.profileImage.path : null,
                    isFollowing: e.isFollowing,
                    isFollowingBack: e.isFollowingBack
                  };
                })
              };
              resolve(
                send.success({
                  message: "User List Fetched",
                  data: data
                })
              );
            } else {
              resolve(
                send.failure({
                  message: "Error Fetching User List"
                })
              );
            }
          }
        }
      );
    });
  }
};
