const auth = require("./auth");
const media = require("./media");
const user = require("./user");
const uploads = require("./uploads");

module.exports = { auth, media, user, uploads };
