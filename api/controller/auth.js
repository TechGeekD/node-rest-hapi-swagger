const mongoose = require("mongoose");
const userModel = mongoose.model("users");
const bcrypt = require("bcrypt");
const handlebars = require("handlebars");

const helper = require(__basedir + "/api/helper");
const send = require("../libs/response-lib");

const apiMailer = "MustWatch <no-reply@mustWatch.com>";

module.exports = {
  register: function(req) {
    console.log("register", JSON.stringify(req.payload, null, 4));

    let saltRounds = 10;
    req.payload.password = bcrypt.hashSync(req.payload.password, saltRounds);

    return new Promise((resolve, reject) => {
      userModel.find(
        {
          $or: [
            {
              username: req.payload.username
            },
            {
              email: req.payload.email
            }
          ]
        },
        function(err, result) {
          if (err) reject(err);
          else {
            if (result.length === 0) {
              let token = helper.api.keyGenerator();
              userModel.create(
                {
                  firstName: req.payload.firstName,
                  lastName: req.payload.lastName,
                  username: req.payload.username,
                  email: req.payload.email,
                  password: req.payload.password,
                  verificationToken: token
                },
                function(err, result) {
                  if (err) reject(err);
                  else {
                    helper.file.read("/views/email-template.html", function(
                      err,
                      html
                    ) {
                      let link = `http://${process.env.machineIp}:${process.env.port}/auth/verifyAccount?token=${token}&name=${
                        req.payload.username
                      }`;
                      let template = handlebars.compile(html);
                      let replacements = {
                        name: req.payload.username,
                        link: link,
                        token: token,
                        verified: false
                      };
                      let htmlToSend = template(replacements);
                      let mailOptions = {
                        from: apiMailer,
                        to: req.payload.email,
                        subject: "Must Watch User Account Activation",
                        html: htmlToSend
                      };

                      helper.api.sendMail(mailOptions, err => {
                        if (!err) {
                          console.log(
                            "***User Created***",
                            req.payload.username
                          );
                          resolve(
                            send.success({
                              message: "User Registered Successfully"
                            })
                          );
                        } else {
                          console.log(
                            "***User Creation Error***",
                            req.payload.username
                          );
                          // mediaSchema.findOneAndDelete(
                          //   {
                          //     $and: [
                          //       {
                          //         username: req.payload.username
                          //       },
                          //       {
                          //         email: req.payload.email
                          //       }
                          //     ]
                          //   },
                          //   function(err, mediaInfo) {
                          //     if (err) next(err);
                          //     else {
                          //       console.log(
                          //         "***User CleanedUP***",
                          //         req.payload.email
                          //       );
                          //     }
                          //   }
                          // );
                          resolve(
                            send.failure({
                              message: "Something Went Wrong"
                            })
                          );
                        }
                      });
                    });
                  }
                }
              );
            } else {
              console.log("***User Exist***", req.payload.username);
              resolve(
                send.failure({
                  message: "User Already Registered"
                })
              );
            }
          }
        }
      );
    });
  },
  authenticate: async req => {
    console.log("authenticate", JSON.stringify(req.payload, null, 4));
    // // console.log("IP ADDR", req.connection.remoteAddress, req.ip, req.ips)
    return new Promise((resolve, reject) => {
      if (req.payload.username && req.payload.password) {
        userModel.findOne(
          {
            $or: [
              {
                username: req.payload.username
              },
              {
                email: req.payload.username
              }
            ]
          },
          (err, userInfo) => {
            if (err) reject(err);
            else {
              if (userInfo !== null) {
                if (
                  bcrypt.compareSync(req.payload.password, userInfo.password)
                ) {
                  if (userInfo.verified !== true) {
                    resolve(
                      send.failure({
                        message: "Verify Account To Continue"
                      })
                    );
                  } else {
                    const token = helper.jwt.sign(userInfo, req);
                    console.log("***User LoggedIn***", req.payload.username);

                    let data = {
                      user: {
                        userId: userInfo._id,
                        fullname: userInfo.fullname,
                        username: userInfo.username,
                        email: userInfo.email
                      },
                      token: token
                    };

                    let response = send.success({
                      message: "Logged In Successfully",
                      data: data
                    });
                    resolve(response);
                  }
                } else {
                  resolve(
                    send.failure({
                      message: "Invalid email/password"
                    })
                  );
                }
              } else {
                resolve(
                  send.failure({
                    message: "Invalid email/password"
                  })
                );
              }
            }
          }
        );
      } else {
        let checkParams = body => {
          let errFlags = [];
          errFlags[0] = !body.username ? true : false;
          errFlags[1] = !body.password ? true : false;
          let err = {};
          if (errFlags[0]) {
            err = {
              ...err,
              ...{
                username: {
                  kind: "required"
                }
              }
            };
          }
          if (errFlags[1]) {
            err = {
              ...err,
              ...{
                password: {
                  kind: "required"
                }
              }
            };
          }
          console.log(err);
          resolve(err);
        };
        resolve(
          send.failure({
            message: "Error Login !",
            data: { errors: checkParams(req.payload) }
          })
        );
      }
    });
  },
  verifyAccount: function(req) {
    console.log("verifyAccount", JSON.stringify(req.query, null, 4));

    return new Promise((resolve, reject) => {
      userModel.findOneAndUpdate(
        {
          $and: [
            {
              username: req.query.name
            },
            { verificationToken: req.query.token }
          ],
          verified: { $ne: true }
        },
        { verified: true },
        function(err, user) {
          if (err) reject(err);
          else {
            if (user !== null) {
              helper.file.read("/views/email-template.html", function(
                err,
                html
              ) {
                if (err) reject(err);
                else {
                  let template = handlebars.compile(html);
                  let replacements = {
                    name: req.query.name,
                    verified: true
                  };
                  let htmlToSend = template(replacements);
                  console.log("Account Verified:", req.query.name);
                  resolve(htmlToSend);
                }
              });
            } else {
              resolve("Invalid URL !");
            }
          }
        }
      );
    });
  }
};
