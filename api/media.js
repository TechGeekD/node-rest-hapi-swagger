const Joi = require("joi");
const RestHapi = require("rest-hapi");
const controller = require("./controller");
const helper = require("./helper");
const send = require("./libs").send;

const headersValidation = Joi.object({
  authorization: Joi.string()
    .description("JWT Token for auth")
    .required()
}).options({ allowUnknown: true });

module.exports = function(server, mongoose, logger) {
  server.route({
    method: "GET",
    path: "/media",
    config: {
      handler: async function(request, h) {
        return await controller.media
          .getAll(request)
          .then(response => {
            return response;
          })
          .catch(err => {
            console.log("ERROR: [GET] media:getAll API: ", err);
            return send.failure({
              message: "Internal Server Error !"
            });
          });
      },
      validate: {
        headers: headersValidation
      },
      auth: "jwt",
      description: "Get List of Media Whom user is following.",
      tags: ["api", "media"],
      plugins: {
        "hapi-swagger": {
          responseMessages: [
            { code: 200, message: "Success" },
            { code: 400, message: "Bad Request" },
            { code: 404, message: "Not Found" },
            { code: 500, message: "Internal Server Error" }
          ]
        }
      }
    }
  });

  server.route({
    method: "POST",
    path: "/media",
    config: {
      handler: async function(request, h) {
        let data = [];
        let fileMetaData = [];
        let file = request.payload.file;
        if (!file[0]) {
          //** hapi js can send array or object for multiple or single file respectively **//
          request.payload.file = [request.payload.file];

          data = [file._data];

          fileMetaData.push({
            ...helper.file.generateMetadata(file._data, request),
            ogFileName: file.hapi.filename
          });
        } else {
          file.forEach(e => {
            data.push(e._data);

            fileMetaData.push({
              ...helper.file.generateMetadata(e._data, request),
              ogFileName: e.hapi.filename
            });
          });
        }

        let mediaTypeCheckFlag = true;
        fileMetaData.forEach(e => {
          let flag = true;

          if (e.fileMeta) {
            flag =
              e.fileMeta.mime.split("/")[0] === "image" || "video"
                ? true
                : false;
          } else {
            flag = false;
          }

          if (!flag) {
            e.err = true;
            mediaTypeCheckFlag = false;
          }
        });

        if (mediaTypeCheckFlag) {
          return await controller.media
            .create(request)
            .then(response => {
              return response;
            })
            .catch(err => {
              console.log("ERROR: [POST] media:create API: ", err);
              return send.failure({
                message: "Internal Server Error !"
              });
            });
        } else {
          let media = fileMetaData.filter(e => {
            return e.err ? true : false;
          });

          return send.failure({
            message: `One or More Media Type Posted Is Not Supported !`,
            data: {
              media: media.map(e => {
                return e.ogFileName;
              })
            }
          });
        }
      },
      payload: {
        output: "stream",
        maxBytes: 30 * 1024 * 1024, // max 30 MB
        parse: true,
        allow: "multipart/form-data",
        timeout: false
      },
      validate: {
        headers: headersValidation,
        payload: {
          file: Joi.any()
            .meta({ swaggerType: "file" })
            .description("Attach File")
            .required()
        }
      },
      auth: "jwt",
      description: "Upload New Media File.",
      tags: ["api", "media"],
      plugins: {
        "hapi-swagger": {
          responseMessages: [
            { code: 200, message: "Success" },
            { code: 400, message: "Bad Request" },
            { code: 404, message: "Not Found" },
            { code: 500, message: "Internal Server Error" }
          ],
          payloadType: "form"
        }
      }
    }
  });

  server.route({
    method: "GET",
    path: "/media/{mediaId}",
    config: {
      handler: async function(request, h) {
        return await controller.media
          .getById(request)
          .then(response => {
            return response;
          })
          .catch(err => {
            console.log("ERROR: [GET] media:getById API: ", err);
            return send.failure({
              message: "Internal Server Error !"
            });
          });
      },
      validate: {
        headers: headersValidation,
        params: {
          mediaId: Joi.string().required()
        }
      },
      auth: "jwt",
      description: "Get List of Media by media id whom user is following.",
      tags: ["api", "media"],
      plugins: {
        "hapi-swagger": {
          responseMessages: [
            { code: 200, message: "Success" },
            { code: 400, message: "Bad Request" },
            { code: 404, message: "Not Found" },
            { code: 500, message: "Internal Server Error" }
          ]
        }
      }
    }
  });

  server.route({
    method: "GET",
    path: "/media/byTimeStamp",
    config: {
      handler: async function(request, h) {
        return await controller.media
          .getByTimeStamp(request)
          .then(response => {
            return response;
          })
          .catch(err => {
            console.log("ERROR: [GET] media:byTimeStamp API: ", err);
            return send.failure({
              message: "Internal Server Error !"
            });
          });
      },
      validate: {
        headers: headersValidation,
        query: {
          timestamp: Joi.date()
            .timestamp("unix")
            .required()
        }
      },
      auth: "jwt",
      description: "Get List of All Media by timestamp whom user is following.",
      tags: ["api", "media"],
      plugins: {
        "hapi-swagger": {
          responseMessages: [
            { code: 200, message: "Success" },
            { code: 400, message: "Bad Request" },
            { code: 404, message: "Not Found" },
            { code: 500, message: "Internal Server Error" }
          ]
        }
      }
    }
  });

  server.route({
    method: "GET",
    path: "/media/paginate",
    config: {
      handler: async function(request, h) {
        return await controller.media
          .paginate(request)
          .then(response => {
            return response;
          })
          .catch(err => {
            console.log("ERROR: [GET] media:paginate API: ", err);
            return send.failure({
              message: "Internal Server Error !"
            });
          });
      },
      validate: {
        headers: headersValidation,
        query: {
          page: Joi.number().required(),
          limit: Joi.number().required()
        }
      },
      auth: "jwt",
      description: "Paginate List of Media whom user is following.",
      tags: ["api", "media"],
      plugins: {
        "hapi-swagger": {
          responseMessages: [
            { code: 200, message: "Success" },
            { code: 400, message: "Bad Request" },
            { code: 404, message: "Not Found" },
            { code: 500, message: "Internal Server Error" }
          ]
        }
      }
    }
  });

  server.route({
    method: "GET",
    path: "/media/lazyLoad",
    config: {
      handler: async function(request, h) {
        return await controller.media
          .lazyLoad(request)
          .then(response => {
            return response;
          })
          .catch(err => {
            console.log("ERROR: [GET] media:lazyLoad API: ", err);
            return send.failure({
              message: "Internal Server Error !"
            });
          });
      },
      validate: {
        headers: headersValidation,
        query: {
          mediaId: Joi.string().required(),
          limit: Joi.number()
            .min(1)
            .required()
        }
      },
      auth: "jwt",
      description: "Paginate List of Media as lazyLoad whom user is following.",
      tags: ["api", "media"],
      plugins: {
        "hapi-swagger": {
          responseMessages: [
            { code: 200, message: "Success" },
            { code: 400, message: "Bad Request" },
            { code: 404, message: "Not Found" },
            { code: 500, message: "Internal Server Error" }
          ]
        }
      }
    }
  });

  server.route({
    method: "PUT",
    path: "/media",
    config: {
      handler: async function(request, h) {
        return await controller.media
          .updateById(request)
          .then(response => {
            return response;
          })
          .catch(err => {
            console.log("ERROR: [PUT] media:updateById API: ", err);
            return send.failure({
              message: "Internal Server Error !"
            });
          });
      },
      validate: {
        headers: headersValidation
      },
      auth: "jwt",
      description: "[WIP] Update user's Media.",
      tags: ["api", "media"],
      plugins: {
        "hapi-swagger": {
          responseMessages: [
            { code: 200, message: "Success" },
            { code: 400, message: "Bad Request" },
            { code: 404, message: "Not Found" },
            { code: 500, message: "Internal Server Error" }
          ]
        }
      }
    }
  });

  server.route({
    method: "DELETE",
    path: "/media/{fileName}",
    config: {
      handler: async function(request, h) {
        return await controller.media
          .deleteById(request)
          .then(response => {
            return response;
          })
          .catch(err => {
            console.log("ERROR: [DELETE] media:deleteById API: ", err);
            return send.failure({
              message: "Internal Server Error !"
            });
          });
      },
      validate: {
        headers: headersValidation,
        params: {
          fileName: Joi.string().required()
        }
      },
      auth: "jwt",
      description: "Delete user's Media.",
      tags: ["api", "media"],
      plugins: {
        "hapi-swagger": {
          responseMessages: [
            { code: 200, message: "Success" },
            { code: 400, message: "Bad Request" },
            { code: 404, message: "Not Found" },
            { code: 500, message: "Internal Server Error" }
          ]
        }
      }
    }
  });
};
