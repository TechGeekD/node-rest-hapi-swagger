const Joi = require("joi");
const RestHapi = require("rest-hapi");
const controller = require("./controller");
const send = require("./libs").send;

module.exports = function(server, mongoose, logger) {
  server.route({
    method: "POST",
    path: "/auth/login",
    config: {
      handler: async function(request, h) {
        return await controller.auth
          .authenticate(request)
          .then(response => {
            return response;
          })
          .catch(err => {
            console.log("ERROR: login API: ", err);
            return send.failure({
              message: "Internal Server Error !"
            });
          });
      },
      auth: false,
      description: "Login into user's account.",
      tags: ["api", "auth", "User", "Password"],
      validate: {
        payload: {
          username: Joi.string()
            .required()
            .description("The user's username"),
          password: Joi.string()
            .required()
            .description("The user's password")
        }
      },
      plugins: {
        "hapi-swagger": {
          responseMessages: [
            { code: 200, message: "Success" },
            { code: 400, message: "Bad Request" },
            { code: 404, message: "Not Found" },
            { code: 500, message: "Internal Server Error" }
          ]
        }
      }
    }
  });

  server.route({
    method: "POST",
    path: "/auth/register",
    config: {
      handler: async function(request, h) {
        return await controller.auth
          .register(request)
          .then(response => {
            return response;
          })
          .catch(err => {
            console.log("ERROR: register API: ", err);
            return send.failure({
              message: "Internal Server Error !"
            });
          });
      },
      validate: {
        payload: {
          username: Joi.string()
            .required()
            .description("The user's username"),
          email: Joi.string()
            .email()
            .required()
            .description("The user's Email ID"),
          password: Joi.string()
            .required()
            .description("The user's password"),
          firstName: Joi.string()
            .required()
            .description("The user's firstName"),
          lastName: Joi.string().description("The user's lastName")
        }
      },
      auth: false,
      description: "Register new user's account.",
      tags: ["api", "auth"],
      plugins: {
        "hapi-swagger": {
          responseMessages: [
            { code: 200, message: "Success" },
            { code: 400, message: "Bad Request" },
            { code: 404, message: "Not Found" },
            { code: 500, message: "Internal Server Error" }
          ]
        }
      }
    }
  });

  server.route({
    method: "GET",
    path: "/auth/verifyAccount",
    config: {
      handler: async function(request, h) {
        return await controller.auth
          .verifyAccount(request)
          .then(response => {
            return response;
          })
          .catch(err => {
            console.log("ERROR: verifyAccount API: ", err);
            return send.failure({
              message: "Internal Server Error !"
            });
          });
      },
      auth: false,
      description: "Verify user's account.",
      validate: {
        query: {
          token: Joi.string()
            .token()
            .required()
            .description("The user's Verification Token"),
          name: Joi.string()
            .required()
            .description("The user's username")
        }
      },
      tags: ["api", "auth"],
      plugins: {
        "hapi-swagger": {
          responseMessages: [
            { code: 200, message: "Success" },
            { code: 400, message: "Bad Request" },
            { code: 404, message: "Not Found" },
            { code: 500, message: "Internal Server Error" }
          ]
        }
      }
    }
  });
};
