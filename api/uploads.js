const Joi = require("joi");
const RestHapi = require("rest-hapi");
const controller = require("./controller");
const send = require("./libs").send;

const headersValidation = Joi.object({
  authorization: Joi.string()
    .description("JWT Token for auth")
    .required()
}).options({ allowUnknown: true });

module.exports = function(server, mongoose, logger) {
  server.route({
    method: "GET",
    path: "/uploads/{fileName}",
    config: {
      handler: async function(request, h) {
        return await controller.uploads
          .fetchMediaUploads(request)
          .then(response => {
            console.log("fetchMediaUploads", response);
            return h.file(__basedir + response);
          })
          .catch(err => {
            console.log("ERROR: [GET] uploads:fetchMediaUploads API: ", err);
            return send.failure({ message: "Interval Server Error !" });
          });
      },
      validate: {
        headers: headersValidation,
        params: {
          fileName: Joi.string()
            .description("Name Of The File")
            .required()
        }
      },
      auth: "jwt",
      description: "Get user's Media.",
      tags: ["api", "media"],
      plugins: {
        "hapi-swagger": {
          responseMessages: [
            { code: 200, message: "Success" },
            { code: 400, message: "Bad Request" },
            { code: 404, message: "Not Found" },
            { code: 500, message: "Internal Server Error" }
          ]
        }
      }
    }
  });

  server.route({
    method: "GET",
    path: "/uploads/profileImage",
    config: {
      handler: async function(request, h) {
        return await controller.uploads
          .fetchProfileImageUploads(request)
          .then(response => {
            console.log("fetchProfileImageUploads", response);
            return h.file(__basedir + response);
          })
          .catch(err => {
            console.log(
              "ERROR: [GET] profileImage:fetchProfileImageUploads API: ",
              err
            );
            return send.failure({ message: "Interval Server Error !" });
          });
      },
      validate: {
        headers: headersValidation,
        query: {
          user: Joi.string().description(
            "UserId of the user whom profile image needed; opting this parameter out will fetch user's own profile Image."
          )
        }
      },
      auth: "jwt",
      description: "Get user's Profile Image.",
      tags: ["api", "media"],
      plugins: {
        "hapi-swagger": {
          responseMessages: [
            { code: 200, message: "Success" },
            { code: 400, message: "Bad Request" },
            { code: 404, message: "Not Found" },
            { code: 500, message: "Internal Server Error" }
          ]
        }
      }
    }
  });

  server.route({
    method: "GET",
    path: "/uploads/profileImage/{fileName}",
    config: {
      handler: async function(request, h) {
        return await controller.uploads
          .fetchProfileImageUploads(request)
          .then(response => {
            console.log("fetchProfileImageUploads", response);
            return h.file(__basedir + response);
          })
          .catch(err => {
            console.log(
              "ERROR: [GET] profileImage:fetchProfileImageUploads API: ",
              err
            );
            return send.failure({ message: "Interval Server Error !" });
          });
      },
      validate: {
        headers: headersValidation
      },
      auth: "jwt",
      description: "Get user's Profile Image.",
      tags: ["api", "media"],
      plugins: {
        "hapi-swagger": {
          responseMessages: [
            { code: 200, message: "Success" },
            { code: 400, message: "Bad Request" },
            { code: 404, message: "Not Found" },
            { code: 500, message: "Internal Server Error" }
          ]
        }
      }
    }
  });
};
