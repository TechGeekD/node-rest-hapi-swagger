let os = require("os");

// console.log("os.networkInterfaces", os.networkInterfaces());

module.exports = {
  getMachineIP: function({ interface, ipFamily = "IPv4", internalIp = false }) {
    let ipAddrArray = [];
    let networkInterfaces = os.networkInterfaces();

    if (networkInterfaces[interface]) {
      networkInterfaces[interface].forEach(property => {
        if (property.family === ipFamily && property.internal === internalIp) {
          ipAddrArray.push(property.address);
        }
      });
    }
    console.log("********************", ipAddrArray, "********************");
    return ipAddrArray.length > 0
      ? ipAddrArray
      : new Error("Invalid Interface or No Ip Found !");
  }
};
