function success({ message, data = null }) {
  return buildResponse(1, ...arguments);
}

function failure({ message, data = null }) {
  return buildResponse(0, ...arguments);
}

function buildResponse(statusCode, { message, data = null }) {
  return {
    statusCode: statusCode,
    message,
    data
  };
}

module.exports = { success, failure };
