const send = require("./response-lib");
const osUtil = require("./os-utility");

module.exports = { send, osUtil };
