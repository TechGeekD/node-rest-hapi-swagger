const Joi = require("joi");
const RestHapi = require("rest-hapi");
const controller = require("./controller");
const helper = require("./helper");
const send = require("./libs").send;

const headersValidation = Joi.object({
  authorization: Joi.string()
    .description("JWT Token for auth")
    .required()
}).options({ allowUnknown: true });

module.exports = function(server, mongoose, logger) {
  server.route({
    method: "GET",
    path: "/user/follow",
    config: {
      handler: async function(request, h) {
        return await controller.user
          .getFollowList(request)
          .then(response => {
            return response;
          })
          .catch(err => {
            console.log("ERROR: [GET] follow:getFollowList API: ", err);
            return send.failure({
              message: "Internal Server Error !"
            });
          });
      },
      auth: "jwt",
      description: "Get List of user's follows.",
      tags: ["api", "follows"],
      validate: {
        headers: headersValidation,
        query: {
          user: Joi.string().description(
            "The userId of user to get list of follows"
          )
        }
      },
      plugins: {
        "hapi-swagger": {
          responseMessages: [
            { code: 200, message: "Success" },
            { code: 400, message: "Bad Request" },
            { code: 404, message: "Not Found" },
            { code: 500, message: "Internal Server Error" }
          ]
        }
      }
    }
  });

  server.route({
    method: "POST",
    path: "/user/follow",
    config: {
      handler: async function(request, h) {
        return await controller.user
          .followUser(request)
          .then(response => {
            return response;
          })
          .catch(err => {
            console.log("ERROR: [POST] follow:followUser API: ", err);
            return send.failure({
              message: "Internal Server Error !"
            });
          });
      },
      validate: {
        headers: headersValidation,
        payload: {
          followId: Joi.string()
            .required()
            .description("The userId of user to follow")
        }
      },
      auth: "jwt",
      description: "Follow new user.",
      tags: ["api", "follows"],
      plugins: {
        "hapi-swagger": {
          responseMessages: [
            { code: 200, message: "Success" },
            { code: 400, message: "Bad Request" },
            { code: 404, message: "Not Found" },
            { code: 500, message: "Internal Server Error" }
          ]
        }
      }
    }
  });

  server.route({
    method: "POST",
    path: "/user/unfollow",
    config: {
      handler: async function(request, h) {
        return await controller.user
          .unFollowUser(request)
          .then(response => {
            return response;
          })
          .catch(err => {
            console.log("ERROR: unfollow:unFollowUser API: ", err);
            return send.failure({
              message: "Internal Server Error !"
            });
          });
      },
      auth: "jwt",
      description: "Unfollow the following user.",
      validate: {
        headers: headersValidation,
        payload: {
          followId: Joi.string()
            .required()
            .description("The userId of user to unfollow")
        }
      },
      tags: ["api", "follows"],
      plugins: {
        "hapi-swagger": {
          responseMessages: [
            { code: 200, message: "Success" },
            { code: 400, message: "Bad Request" },
            { code: 404, message: "Not Found" },
            { code: 500, message: "Internal Server Error" }
          ]
        }
      }
    }
  });

  server.route({
    method: "GET",
    path: "/user/profileInfo",
    config: {
      handler: async function(request, h) {
        return await controller.user
          .getProfileInfo(request)
          .then(response => {
            return response;
          })
          .catch(err => {
            console.log("ERROR: profileInfo:getProfileInfo API: ", err);
            return send.failure({
              message: "Internal Server Error !"
            });
          });
      },
      auth: "jwt",
      description: "Get User's Profile Information.",
      validate: {
        headers: headersValidation,
        query: {
          user: Joi.string().description(
            "The userId of user whom profile info is needed."
          )
        }
      },
      tags: ["api", "follows"],
      plugins: {
        "hapi-swagger": {
          responseMessages: [
            { code: 200, message: "Success" },
            { code: 400, message: "Bad Request" },
            { code: 404, message: "Not Found" },
            { code: 500, message: "Internal Server Error" }
          ]
        }
      }
    }
  });

  server.route({
    method: "POST",
    path: "/user/profileImage",
    config: {
      handler: async function(request, h) {
        let data = request.payload.profileImage._data;
        let fileMetaData = helper.file.generateMetadata(data, request).fileMeta;

        mediaTypeCheckFlag = fileMetaData
          ? fileMetaData.mime.split("/")[0] === "image" && "video"
            ? true
            : false
          : false;

        if (mediaTypeCheckFlag) {
          return await controller.user
            .setProfileImage(request)
            .then(response => {
              return response;
            })
            .catch(err => {
              console.log("ERROR: profileImage:setProfileImage API: ", err);
              return send.failure({
                message: "Internal Server Error !"
              });
            });
        } else {
          return send.failure({
            message: `That Media Type Is Not Supported !`
          });
        }
      },
      auth: "jwt",
      description: "Set Or Remove User's Profile Image.",
      payload: {
        output: "stream",
        maxBytes: 30 * 1024 * 1024, // max 30 MB
        parse: true,
        allow: "multipart/form-data",
        timeout: false
      },
      validate: {
        headers: headersValidation,
        payload: {
          profileImage: Joi.any()
            .meta({ swaggerType: "file" })
            .description("Attach File")
        },
        query: {
          remove: Joi.boolean()
        }
      },
      tags: ["api", "profile"],
      plugins: {
        "hapi-swagger": {
          responseMessages: [
            { code: 200, message: "Success" },
            { code: 400, message: "Bad Request" },
            { code: 404, message: "Not Found" },
            { code: 500, message: "Internal Server Error" }
          ],
          payloadType: "form"
        }
      }
    }
  });

  server.route({
    method: "GET",
    path: "/user/list",
    config: {
      handler: async function(request, h) {
        return await controller.user
          .getUsersList(request)
          .then(response => {
            return response;
          })
          .catch(err => {
            console.log("ERROR: [GET] follow:getUsersList API: ", err);
            return send.failure({
              message: "Internal Server Error !"
            });
          });
      },
      auth: "jwt",
      description: "Get List of user.",
      tags: ["api", "follows"],
      validate: {
        headers: headersValidation
      },
      plugins: {
        "hapi-swagger": {
          responseMessages: [
            { code: 200, message: "Success" },
            { code: 400, message: "Bad Request" },
            { code: 404, message: "Not Found" },
            { code: 500, message: "Internal Server Error" }
          ]
        }
      }
    }
  });
};
