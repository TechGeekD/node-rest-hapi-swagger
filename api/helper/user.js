const mongoose = require("mongoose");
const userModel = mongoose.model("users");
const ObjectId = mongoose.Schema.Types.ObjectId;

module.exports = {
  validateUserId: id => {
    return new Promise((resolve, reject) => {
      userModel.findById(id, function(err, result) {
        if (err) reject(err);
        else {
          if (result !== null) resolve(true);
          else resolve(false);
        }
      });
    });
  },
  getObjectIdDetails: result => {
    let id = result._id.toString(),
      ctr = 0;
    let _objectId = {
      timestamp: parseInt(id.slice(ctr, (ctr += 8)), 16),
      machineID: parseInt(id.slice(ctr, (ctr += 6)), 16),
      processID: parseInt(id.slice(ctr, (ctr += 4)), 16),
      counter: parseInt(id.slice(ctr, (ctr += 6)), 16)
    };
    console.log(JSON.stringify(_objectId, null, 4));
    return _objectId;
  }
};
