const api = require("./api");
const jwt = require("./jwt");
const file = require("./file");
const user = require("./user");
const media = require("./media");

module.exports = { api, jwt, file, user, media };
