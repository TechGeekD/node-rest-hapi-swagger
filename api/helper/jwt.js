const jwt = require("jsonwebtoken");

module.exports = {
  getUserId: req => {
    let userId,
      secret = "secret";
    jwt.verify(req.headers["authorization"], secret, function(err, decoded) {
      if (err) {
        userId = err;
      } else {
        userId = decoded.id;
      }
    });
    return userId;
  },
  sign: (userInfo, req) => {
    let secret = "secret";
    return jwt.sign(
      {
        id: userInfo._id
      },
      secret
      // {
      //   expiresIn: "1h"
      // }
    );
  }
};
