const fs = require("fs");
const uuidv4 = require("uuid/v4");
const fileType = require("file-type");

module.exports = {
  read: (path, callback) => {
    path = __basedir + path;
    fs.readFile(path, { encoding: "utf-8" }, function(err, file) {
      if (err) {
        throw err;
        callback(err);
      } else {
        callback(null, file);
      }
    });
  },
  write: (path, file, callback) => {
    path = __basedir + path;
    fs.writeFile(path, file, err => {
      if (err) callback(err);
      callback(`The file has been saved at ${path} !`);
    });
  },
  generateMetadata: (data, req) => {
    let fileMeta = fileType(data);
    let fileName = fileMeta
      ? `${req.auth.credentials.id}_${uuidv4()}.${fileMeta.ext}`
      : null;
    return { fileName, fileMeta };
  }
};
