const fs = require("fs");
const _ = require("lodash");

const VisualRecognitionV3 = require("watson-developer-cloud/visual-recognition/v3");

const visualRecognition = new VisualRecognitionV3({
  version: "2018-03-19",
  api_key: process.env.WATSON_VisualRecognition
});

module.exports = {
  verifyContent: (_params, media, uploadPath = null, cb) => {
    let path = __basedir + "/uploads/temp/";
    let originPath = uploadPath
      ? __basedir + uploadPath
      : __basedir + "/uploads/";
    let media_file = fs.createReadStream(path + media);
    let classifier_ids = ["explicit"];

    let params = {
      images_file: media_file,
      classifier_ids: classifier_ids
    };

    let deleteMedia = (path, _err) => {
      fs.unlink(path, err => {
        console.log(media + " Removed!");
      });
      cb(_err);
    };

    let moveMedia = (from, to) => {
      fs.rename(from, to, err => {
        if (err) cb(err);
        console.log(media + " Moved!");
      });
      cb();
    };

    if (!_params.mediaType) {
      moveMedia(path + media, originPath + media);
    } else if (_params.mediaType) {
      visualRecognition.classify(params, function(err, response) {
        if (err) {
          console.log("watsonError", err);
          cb("Something Went Wrong");
        } else {
          let verify = _.get(response, "images[0].classifiers[0].classes[0]");
          console.log("watson", verify);
          // console.log("watson", JSON.stringify(response, null, 2));
          if (verify.class === "not explicit" && verify.score > 0.75) {
            moveMedia(path + media, originPath + media);
          } else {
            let _err =
              "Some Media Rejected: Uploaded Media Contains Explicit Content";
            deleteMedia(path + media, _err);
          }
        }
      });
    }
  },
  removeMedia: (fileName, path = null, cb) => {
    let folder = path ? __basedir + path : __basedir + "/uploads/";

    fs.readdir(folder, (err, files) => {
      if (files) {
        let flag = false;
        files.forEach(file => {
          if (file.includes(fileName)) {
            flag = true;
            fs.unlink(folder + file, err => {
              cb(err);
            });
          }
        });
        if (!flag) {
          console.log("Media Not Found: ", fileName);
          cb("err");
        }
      }
    });
  }
};
