const nodemailer = require("nodemailer");

const transporter = nodemailer.createTransport({
  host: "smtp.gmail.com",
  port: 465,
  auth: {
    user: process.env.GMAIL_ID,
    pass: process.env.GMAIL_APP_PASSWORD
  }
});

module.exports = {
  sanitization: req => {
    let length = false;
    Object.keys(req.body).forEach(key => {
      req.body[key] = req.sanitize(req.body[key]);
      req.body[key].length === 0 ? (length = true) : null;
    });
    return length;
  },
  sendMail: (options, cb) => {
    const mailOptions = {
      from: options.from,
      to: options.to,
      subject: options.subject
    };

    if (options.text) {
      mailOptions["text"] = options.text;
    }

    if (options.html) {
      mailOptions["html"] = options.html;
    }

    transporter.sendMail(mailOptions, function(error, info) {
      if (error) {
        console.log(error);
        cb(error);
      } else {
        console.log("Email sent: " + info.response);
        cb();
      }
    });
  },
  keyGenerator: ({ limit = 20 } = {}) => {
    const keyElem =
      "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    let randomKey = "";

    let generate = () => {
      for (var i = 0; i < limit; i++) {
        randomKey += keyElem.charAt(Math.floor(Math.random() * keyElem.length));
      }
    };

    generate();
    return randomKey;
  }
};
