let Hapi = require("hapi");
let mongoose = require("mongoose");
let RestHapi = require("rest-hapi");
let osUtil = require("./api/libs").osUtil;

// console.log("process.env", process.env);
const dotenv = require("dotenv").config();

if (dotenv.error) {
  throw new Error(String(dotenv.error));
}

let port = 3000;
let machineIp = osUtil.getMachineIP({ interface: "Local Area Connection" })[0];
process.env.machineIp = machineIp ? machineIp : process.env.API_URL;
process.env.port = port;

global.__basedir = __dirname;

const validate = async function(decoded, request) {
  const helper = require("./api/helper");
  let isValid = { isValid: false };
  request.payload = { ...request.payload };
  if (decoded.id) {
    await helper.user
      .validateUserId(decoded.id)
      .then(_isValid => {
        // request.payload.userId = decoded.id;
        console.log("validate", decoded);
        isValid = {
          isValid: _isValid
        };
      })
      .catch(err => {
        isValid = {
          isValid: false
        };
      });

    return isValid;
  } else {
    return isValid;
  }
};

async function api() {
  try {
    let server = Hapi.Server({
      port: port,
      routes: {
        validate: {
          failAction: async (request, h, err) => {
            RestHapi.logger.error(err);
            throw err;
          }
        }
      }
    });

    await server.register(require("hapi-auth-jwt2"));

    server.auth.strategy("jwt", "jwt", {
      key: "secret",
      validate: validate,
      verifyOptions: { algorithms: ["HS256"] }
    });

    server.auth.default("jwt");

    let config = {
      appTitle: "Must Watch",
      enableTextSearch: true,
      logRoutes: true,
      docExpansion: "list",
      swaggerHost: `${process.env.machineIp}:${process.env.port}`,
      headers: ["*"],
      mongo: {
        URI: process.env.MONGO_URI
      },
      enableAuditLog: false
    };

    await server.register({
      plugin: RestHapi,
      options: {
        mongoose: mongoose,
        config: config
      }
    });

    await server.start();

    RestHapi.logUtil.logActionComplete(
      RestHapi.logger,
      "Server Initialized",
      server.info
    );

    return server;
  } catch (err) {
    console.log("Error starting server:", err);
  }
}

module.exports = api();
