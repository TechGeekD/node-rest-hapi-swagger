"user strict";
//Set up mongoose connection
const mongoose = require("mongoose");
// const mongoDB = 'mongodb://ds125680.mlab.com:25680/dummy';
const mongoDB = "mongodb://user:12345@192.168.0.80:27017/mustWatch?authSource=admin&w=1";
// const mongoDB = 'mongodb://mongo:27017/mustWatch'; // if running under docker

mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
module.exports = mongoose;
