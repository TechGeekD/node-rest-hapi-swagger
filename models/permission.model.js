module.exports = function(mongoose) {
  let modelName = "permission";
  let Types = mongoose.Schema.Types;
  let Schema = new mongoose.Schema({
    name: {
      type: Types.String,
      required: true
    },
    description: {
      type: Types.String
    }
  });
  Schema.statics = {
    collectionName: modelName,
    routeOptions: {
      allowRead: false,
      allowCreate: false,
      allowUpdate: false,
      allowDelete: false,
      associations: {
        users: {
          allowAdd: false,
          allowRemove: false,
          allowRead: false,
          type: "MANY_MANY",
          alias: "user",
          model: "users",
          linkingModel: "user_permission"
        },
        medias: {
          allowAdd: false,
          allowRemove: false,
          allowRead: false,
          type: "MANY_MANY",
          alias: "media",
          model: "media",
          linkingModel: "media_permission"
        },
        roles: {
          allowAdd: false,
          allowRemove: false,
          allowRead: false,
          type: "MANY_MANY",
          alias: "role",
          model: "role",
          linkingModel: "role_permission"
        },
        groups: {
          allowAdd: false,
          allowRemove: false,
          allowRead: false,
          type: "MANY_MANY",
          alias: "group",
          model: "group",
          linkingModel: "group_permission"
        }
      }
    }
  };

  return Schema;
};
