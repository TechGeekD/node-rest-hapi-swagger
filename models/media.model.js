module.exports = function(mongoose) {
  let modelName = "media";
  let Types = mongoose.Schema.Types;
  let Schema = new mongoose.Schema(
    {
      userId: {
        type: Types.ObjectId,
        required: true,
        ref: "users"
      },
      name: {
        type: Types.String,
        required: true
      },
      fileName: {
        type: Types.String,
        required: true
      },
      type: {
        type: Types.String,
        default: "link"
      },
      path: {
        type: Types.String,
        required: true
      },
      timestamp: {
        type: Types.Number,
        require: true
      },
      thumbnail: {
        type: Types.String,
        required: false
      },
      source: {
        type: Types.String,
        default: "localsource",
        required: false
      }
    },
    { collection: modelName }
  );

  Schema.statics = {
    collectionName: modelName,
    routeOptions: {
      allowRead: false,
      allowCreate: false,
      allowUpdate: false,
      allowDelete: false,
      associations: {
        users: {
          allowAdd: false,
          allowRemove: false,
          allowRead: false,
          type: "MANY_ONE",
          alias: "user",
          foreignField: "_id",
          model: "users"
        },
        permissions: {
          allowAdd: false,
          allowRemove: false,
          allowRead: false,
          type: "MANY_MANY",
          alias: "permission",
          model: "permission",
          linkingModel: "media_permission"
        }
      }
    }
  };

  return Schema;
};
