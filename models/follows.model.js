module.exports = function(mongoose) {
  let modelName = "follows";
  let Types = mongoose.Schema.Types;
  let Schema = new mongoose.Schema(
    {
      userId: {
        type: Types.ObjectId,
        ref: "users",
        required: true
      },
      followingId: [
        {
          type: Types.ObjectId,
          ref: "users",
          required: true
        }
      ],
      followersId: [
        {
          type: Types.ObjectId,
          ref: "users",
          required: true
        }
      ]
    },
    { collection: modelName }
  );

  Schema.statics = {
    collectionName: modelName,
    routeOptions: {
      allowRead: false,
      allowCreate: false,
      allowUpdate: false,
      allowDelete: false,
      associations: {
        users: {
          allowAdd: false,
          allowRemove: false,
          allowRead: false,
          type: "ONE_MANY",
          alias: "user",
          foreignField: "_id",
          model: "users"
        },
        permissions: {
          allowAdd: false,
          allowRemove: false,
          allowRead: false,
          type: "MANY_MANY",
          alias: "permission",
          model: "permission",
          linkingModel: "role_permission"
        }
      }
    }
  };

  return Schema;
};
