module.exports = function(mongoose) {
  let modelName = "role";
  let Types = mongoose.Schema.Types;
  let Schema = new mongoose.Schema(
    {
      name: {
        type: Types.String,
        enum: ["Account", "Admin", "SuperAdmin"],
        required: true
      },
      description: {
        type: Types.String
      }
    },
    { collection: modelName }
  );

  Schema.statics = {
    collectionName: modelName,
    routeOptions: {
      allowRead: false,
      allowCreate: false,
      allowUpdate: false,
      allowDelete: false,
      associations: {
        users: {
          allowAdd: false,
          allowRemove: false,
          allowRead: false,
          type: "ONE_MANY",
          alias: "user",
          foreignField: "role",
          model: "users"
        },
        permissions: {
          allowAdd: false,
          allowRemove: false,
          allowRead: false,
          type: "MANY_MANY",
          alias: "permission",
          model: "permission",
          linkingModel: "role_permission"
        }
      }
    }
  };

  return Schema;
};
